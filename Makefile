BASENAME=main

.PHONY: default
default: clean compile


compile:
	pdflatex $(BASENAME)
	biber $(BASENAME)
	pdflatex $(BASENAME)
	pdflatex $(BASENAME)


clean: clean
	find . -name '*synctex.gz(busy)' -print0 | xargs -0 rm
	find . -name '*-blx.bib' -print0 | xargs -0 rm
	find . -name '*.acn' -print0 | xargs -0 rm
	find . -name '*.acn' -print0 | xargs -0 rm
	find . -name '*.acr' -print0 | xargs -0 rm
	find . -name '*.alg' -print0 | xargs -0 rm
	find . -name '*.aux' -print0 | xargs -0 rm
	find . -name '*.bbl' -print0 | xargs -0 rm
	find . -name '*.bcf' -print0 | xargs -0 rm
	find . -name '*.bla' -print0 | xargs -0 rm
	find . -name '*.blg' -print0 | xargs -0 rm
	find . -name '*.cut' -print0 | xargs -0 rm
	find . -name '*.dvi' -print0 | xargs -0 rm
	find . -name '*.fls' -print0 | xargs -0 rm
	find . -name '*.glg' -print0 | xargs -0 rm
	find . -name '*.glo' -print0 | xargs -0 rm
	find . -name '*.glo' -print0 | xargs -0 rm
	find . -name '*.gls' -print0 | xargs -0 rm
	find . -name '*.glsdefs' -print0 | xargs -0 rm
	find . -name '*.glx' -print0 | xargs -0 rm
	find . -name '*.gxg' -print0 | xargs -0 rm
	find . -name '*.gxs' -print0 | xargs -0 rm
	find . -name '*.idx' -print0 | xargs -0 rm
	find . -name '*.ilg' -print0 | xargs -0 rm
	find . -name '*.ind' -print0 | xargs -0 rm
	find . -name '*.ist' -print0 | xargs -0 rm
	find . -name '*.loa' -print0 | xargs -0 rm
	find . -name '*.lof' -print0 | xargs -0 rm
	find . -name '*.log' -print0 | xargs -0 rm
	find . -name '*.lol' -print0 | xargs -0 rm
	find . -name '*.lot' -print0 | xargs -0 rm
	find . -name '*.out' -print0 | xargs -0 rm
	find . -name '*.out.ps' -print0 | xargs -0 rm
	find . -name '*.synctex.gz' -print0 | xargs -0 rm
	find . -name '*.tdo' -print0 | xargs -0 rm
	find . -name '*.toc' -print0 | xargs -0 rm
	find . -name '*.xdy' -print0 | xargs -0 rm
	find . -name '*run.xml' -print0 | xargs -0 rm
